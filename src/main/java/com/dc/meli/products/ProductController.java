package com.dc.meli.products;

import com.dc.meli.shared.Paging;
import com.dc.meli.shared.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Arrays;

@RestController
@RequestMapping("${base.url}")
public class ProductController {

    @GetMapping("/products")
    public Response getProducts() {

        Response response = new Response();

        ProductModel product = new ProductModel();
        product.setId("MLA916559270");
        product.setTitle("0026 Notebook Toshiba Satellite Pro M15-s405");
        product.setCondition("used");
        product.setThumbnail_id("712825-MLA71564339721_092023");
        product.setCatalog_product_id("MLA22265813");
        product.setListing_type_id("gold_special");
        product.setPermalink("https://articulo.mercadolibre.com.ar/MLA-916559270-0026-notebook-toshiba-satellite-pro-m15-s405-_JM");
        product.setBuying_mode("buy_it_now");
        product.setSite_id("MLA");
        product.setCategory_id("MLA1652");
        product.setDomain_id("MLA-NOTEBOOKS");
        product.setThumbnail("http://http2.mlstatic.com/D_712825-MLA71564339721_092023-I.jpg");
        product.setCurrency_id("ARS");
        product.setOrder_backend(1);
        product.setPrice(BigDecimal.valueOf(400));
        product.setOriginal_price(null);
        product.setSale_price(null);
        product.setSold_quantity(0);
        product.setAvailable_quantity(1);
        product.setOfficial_store_id(null);
        product.setUse_thumbnail_id(true);
        product.setAccepts_mercadopago(true);

        response.setResults(new ProductModel[]{product});

        Paging paging = new Paging();
        paging.setTotal(1240);
        paging.setPrimary_results(1000);
        paging.setOffset(0);
        paging.setLimit(2);
        response.setPaging(paging);

        return response;
    }

}
