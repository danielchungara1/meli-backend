package com.dc.meli.products;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductModel {
    private String id;
    private String title;
    private String condition;
    private String thumbnail_id;
    private String catalog_product_id;
    private String listing_type_id;
    private String permalink;
    private String buying_mode;
    private String site_id;
    private String category_id;
    private String domain_id;
    private String thumbnail;
    private String currency_id;
    private Integer order_backend;
    private BigDecimal price;
    private BigDecimal original_price;
    private BigDecimal sale_price;
    private Integer sold_quantity;
    private Integer available_quantity;
    private String official_store_id;
    private Boolean use_thumbnail_id;
    private Boolean accepts_mercadopago;
}
