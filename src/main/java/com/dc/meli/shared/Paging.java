package com.dc.meli.shared;

import lombok.Data;

@Data
public class Paging {

    private Integer total;
    private Integer primary_results;
    private Integer offset;
    private Integer limit;

}
