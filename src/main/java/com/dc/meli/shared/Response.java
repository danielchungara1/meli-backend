package com.dc.meli.shared;

import lombok.Data;

@Data
public class Response {

    private Paging paging;
    private Object[] results;

}
