package com.dc.meli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeliBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MeliBackendApplication.class, args);
    }

}
